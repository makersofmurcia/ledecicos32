#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void blink_task(void *pvParameter);

#ifdef __cplusplus
}
#endif

/* Can run 'make menuconfig' to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO
