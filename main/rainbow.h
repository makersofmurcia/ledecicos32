#pragma once


#ifdef __cplusplus
extern "C" {
#endif

#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)
void rainbow(void *pvParameters);

#ifdef __cplusplus
}
#endif
